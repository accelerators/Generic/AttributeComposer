/*
  File:        ExpParser.cpp
  Description: A simple expression parser
  Author:      J-L PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

//=============================================================================
//
// $Author: pons $
//
// $Revision: 1.2 $
//
// $Log: $
//
//=============================================================================

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include <errno.h>

namespace AttributeComposer_ns
{
class ExpParser;
}

#include <ExpParser.h>

namespace AttributeComposer_ns
{

#include <CalcFunc.h>

// -------------------------------------------------------
// Utils functions
// -------------------------------------------------------

// Add a character to a string
static void stradd(char *s,char c)
{
  int l=(int)strlen(s);
  s[l]=c;
  s[l+1]=0;
}

// free a tree
static void free_tree(ETREE *t) {
  if(t->left!=NULL)  free_tree(t->left);
  if(t->right!=NULL) free_tree(t->right);
  free(t);
}

static void safe_free_tree(ETREE **t)
{
  if(*t!=NULL) {
    free_tree(*t);
    *t=NULL;
  }
}

// -------------------------------------------------------
// ExpParser
// -------------------------------------------------------

ExpParser::ExpParser(AttributeComposer *parent) {

  this->parent = parent;
  evalTree=NULL;
  memset(expr,0,sizeof(expr));
  strcpy(name,"");
  strcpy(status,"");
  devices.clear();

}

// -------------------------------------------------------

ExpParser::~ExpParser() {

  safe_free_tree(&evalTree);
  for(int i=0;i<(int)devices.size();i++)
    delete devices[i].ds;
  devices.clear();

}

// -------------------------------------------------------

char *ExpParser::GetName() {
  return name;
}

// -------------------------------------------------------

void ExpParser::SetExpression(char *expr) {
  if(strlen(expr)>sizeof(this->expr)) 
    SetError((char *)"Expression too long");
  memset(this->expr,0,sizeof(this->expr));
  strcpy(this->expr,expr);
}

// -------------------------------------------------------

char *ExpParser::GetExpression() {
  return expr;
}

// -------------------------------------------------------
// Return true if next string match given string 

bool ExpParser::Match(string word)
{
  char s[128];
  int i;
  int lg = word.length();
  s[0]=0;
  for(i=current;i<current+lg;i++)
    stradd(s,expr[i]);

  return strcasecmp(s,word.c_str())==0;
}

// -------------------------------------------------------

void ExpParser::AV()
{
  // Return fisrt significative char in the string to analyse 
  do {
    current++;
    EC=expr[current];
  } while (current<exprLgth && (EC==' ' || EC=='\t'));
}

void ExpParser::AV(int n) {
  for(int i=0;i<n;i++)
    AV();
}

// -------------------------------------------------------

void ExpParser::SetError(char *err,int p) {

  char errMsg[512];

  if(p>=0)
    sprintf(errMsg,"%s at %d",err,p);
  else
    strcpy(errMsg,err);

  Tango::Except::throw_exception(	    
      (const char *)"ExpParser::error",
      (const char *)errMsg,
      (const char *)"ExpParser::SetError"); 

}


// -------------------------------------------------------

void ExpParser::AddNode(CALCFN fn , ETREE_NODE info ,
                        ETREE **t,ETREE *left,ETREE *right) {

  // Add node into the evaluation tree
  ETREE *elem;
  elem=(ETREE *)malloc(sizeof(ETREE));
  elem->calc_fn=fn;
  elem->info=info;
  elem->left=left;
  elem->right=right;

  *t=elem;

}

// -------------------------------------------------------
// Gramar functions
// -------------------------------------------------------

void ExpParser::ReadDouble(double *R)
{
  char S[128];
  char ex[128];
  int  c;
  int  p;
  int  nega;
  int  n;

  S[0]=0;
  ex[0]=0;
  p=current;

  if( Match("0x") ) {
    // We have an hexadecimal number
    ReadInteger(&n);
    *R = (double)n;
    return;
  }

  do {
    stradd(S,EC);
    AV();
  } while ( (EC>='0' && EC<='9') || EC=='.' );

  if (EC=='E' || EC=='e') {
    AV();
    nega=false;

    if (EC=='-') {
      nega=true;
      AV();
    }

    if (EC=='+') {
      AV();
    }

    while (EC>='0' && EC<='9')
    {
      stradd(ex,EC);
      AV();
    }
    c=sscanf(ex,"%d",&n);
    if (c==0) { SetError((char *)"Incorrect exposant in number",p);return; }
    if (nega) { strcat(S,"e-");strcat(S,ex);}
    else      { strcat(S,"e");strcat(S,ex); }
  }

  c=sscanf(S,"%lf",R);  
  if (c==0) SetError((char *)"Incorrect number",p);

}

// -------------------------------------------------------

bool ExpParser::IsLetter(char c) {

  return (EC>='A' && EC<='Z') || (EC>='a' && EC<='z') || (EC=='_');

}

// -------------------------------------------------------

void ExpParser::ReadInteger(int *R)
{
  char S[128];
  char ex[128];
  int  c;
  int  p;
  int  isHexa;

  S[0]=0;
  ex[0]=0;
  p=current;

  isHexa = Match("0x");
  if(isHexa) {

    stradd(S,EC);AV();
    stradd(S,EC);AV();

    do {
      stradd(S,EC);
      AV();
    } while ( (EC>='0' && EC<='9') || (EC>='A' && EC<='F') || (EC>='a' && EC<='f') );

  } else {

    do {
      stradd(S,EC);
      AV();
    } while ( (EC>='0' && EC<='9') );
  
  }
  
  if( strlen(S)==0 ) SetError((char *)"Incorrect integer number",p);

  if( isHexa ) {
    errno=0;
    *R = strtol(S,NULL,16);
    if(errno!=0) {
      cerr << "Invalid hexadecimal number:" << S << endl;
      SetError((char *)"Incorrect hexadecimal number",p);
    }
  } else {
    c=sscanf(S,"%d",R);
    if (c==0) {
      cerr << "Invalid number:" << S << endl;
      SetError((char *)"Incorrect integer number",p);
    }
  }
  
}

// -------------------------------------------------------

void ExpParser::ReadString( char *str ) {

  int i=0;
  str[0] = 0;
  while( EC!='\'' && EC!=0 ) {

    if(Match("\\'")) {
      stradd(str,'\'');
      current+=2;
    } else {
      stradd(str,EC);
      current++;
    }
    EC = expr[current];

    i++;
    if(i>=MAXLENGHT) SetError((char *)"String too long");

  }

  if(EC==0) SetError((char *)"Unterminated string");

}

// -------------------------------------------------------

void ExpParser::ReadName( char *name ) {

  int i=0;
  int p;
  p=current;
  
  /*
  if( !IsLetter(EC) ) {
    error = true;
    SetError((char *)"Name expected",p);
    return;
  }
  */

  name[0]=0;
  do {

    stradd(name,EC);

    current++;
    EC=expr[current];

    i++;
    if(i>=MAXLENGHT) SetError((char *)"Variable name too long",p);

  } while (
     (EC>='A' && EC<='Z') || 
     (EC>='a' && EC<='z') ||
     (EC>='0' && EC<='9') ||
     (EC=='_'));

  // Jump to the next significative char
  while(current<exprLgth && (EC==' ' || EC=='\t')) {
    current++;
    EC=expr[current];
  }

}

// -------------------------------------------------------

void ExpParser::ReadAttName( char *name ) {

  int i=0;
  int p;
  p=current;
  
  /*
  if( !IsLetter(EC) ) {
    error = true;
    SetError((char *)"Name expected",p);
    return;

  }
  */

  name[0]=0;
  while( EC!='\'' ) {

    stradd(name,EC);

    current++;
    EC=expr[current];

    i++;
    if(i>=MAXLENGHT) SetError((char *)"Attribute name too long",p);

  }

}

// -------------------------------------------------------

void ExpParser::ReadTerm(ETREE **node) {

  ETREE *l_t=NULL;
  ETREE *r_t=NULL;
  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  switch(EC) {

    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '.':  ReadDouble(&(elem.constant));
      AddNode( OPER_DOUBLE , elem , node , NULL , NULL);
      break;

    case '(' : AV();
      ReadExpression(node);
      if (EC!=')') SetError((char *)") expected",current);
      AV();
      break;

      // -------------------------------------------------------
      // unary operator
      // -------------------------------------------------------

    case '-' :AV();
      ReadTerm(&l_t);
      AddNode( OPER_MINUS1 , elem , node , l_t , NULL);
      break;

    case '~' :AV();
      ReadTerm(&l_t);
      AddNode( OPER_NEG , elem , node , l_t , NULL);
      break;

    case 'N':
    case 'n': if( Match("not") ) {
        AV(3);
        ReadTerm(&l_t);
        AddNode( OPER_NOT , elem , node , l_t , NULL);
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

      // -------------------------------------------------------
      // Math functions
      // -------------------------------------------------------

    case 'a':
    case 'A': if ( Match("abs(") ) {
        AV(4);
        ReadExpression(&l_t);
        AddNode( OPER_ABS , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("asin(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_ASIN , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("acos(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_ACOS , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else  if ( Match("atan(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_ATAN , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else  if ( Match("avg(") ) {
        ParseFn("avg",OPER_AVG,node);
      } else if ( Match("avgAtt(") ) {
        ParseAttFn("avgAtt",OPER_AVGATT,node);
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'S':
    case 's': if ( Match("sin(") ) {
        AV(4);
        ReadExpression(&l_t);
        AddNode( OPER_SIN , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("sqrt(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_SQRT , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("sinh(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_SINH , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("sum(") ) {
        ParseFn("sum",OPER_SUM,node);
      } else if ( Match("std(") ) {
        ParseFn("std",OPER_STD,node);
      } else if ( Match("sumAtt(") ) {
        ParseAttFn("sumAtt",OPER_SUMATT,node);
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'C':
    case 'c': if ( Match("cos(") ) {
        AV(4);
        ReadExpression(&l_t);
        AddNode( OPER_COS , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("cosh(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_COSH , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("contains(") ) {
        int idx;
        AV(9);
        ReadInteger(&idx);
        if (EC!=',') SetError((char *)", expected",current);
        AV();
        if (EC!='\'') SetError((char *)"' expected",current);
        AV();
        elem.attinfo.idx = idx;
        ReadString(elem.attinfo.param.searchStr);
        if (EC!='\'') SetError((char *)"' expected",current);
        AV();
        AddNode( OPER_CONTAINS , elem , node , NULL , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'E':
    case 'e': if ( Match("exp(") ) {
        AV(4);
        ReadExpression(&l_t);
        AddNode( OPER_EXP , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'F':
    case 'f': if ( Match("fact(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_FACT , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'I':
    case 'i': if ( Match("inv(") ) {
        AV(4);
        ReadExpression(&l_t);
        AddNode( OPER_INV , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("icontains(") ) {
        int idx;
        AV(10);
        ReadInteger(&idx);
        if (EC!=',') SetError((char *)", expected",current);
        AV();
        if (EC!='\'') SetError((char *)"' expected",current);
        AV();
        elem.attinfo.idx = idx;
        ReadString(elem.attinfo.param.searchStr);
        if (EC!='\'') SetError((char *)"' expected",current);
        AV();
        AddNode( OPER_ICONTAINS , elem , node , NULL , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'L':
    case 'l': if ( Match("ln(") ) {
        AV(3);
        ReadExpression(&l_t);
        AddNode( OPER_LN , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("log2(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_LOG2 , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("log10(") ) {
        AV(6);
        ReadExpression(&l_t);
        AddNode( OPER_LOG10 , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("length(") ) {
        ParseFn("length",OPER_LENGTH,node);
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'V':
    case 'v': if ( Match("val(") ) {
        int idx;
        int vidx;
        AV(4);
        ReadInteger(&idx);
        if (EC!=',') SetError((char *)", expected",current);
        AV();
        ReadInteger(&vidx);
        elem.attinfo.idx = idx;
        elem.attinfo.valIdx = vidx;
        AddNode( OPER_VAL , elem , node , NULL , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'T':
    case 't': if ( Match("tan(") ) {
        AV(4);
        ReadExpression(&l_t);
        AddNode( OPER_TAN , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("tanh(") ) {
        AV(5);
        ReadExpression(&l_t);
        AddNode( OPER_TANH , elem , node , l_t , NULL);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'x':
    case 'X': if ( Match("xattr(") ) {
        AV(6);
        if (EC!='\'') SetError((char *)"' expected",current);
        AV();
        ReadAttName((char *)elem.name);
        AddNode( OPER_XATTR , elem , node , NULL , NULL);
        if (EC!='\'') SetError((char *)"' expected",current);
        AV();
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

      // -------------------------------------------------------
      // Constants
      // -------------------------------------------------------

    case 'P':
    case 'p': if ( Match("pi") ) {
        AV(2);
        elem.constant=3.14159265358979323846;
        AddNode( OPER_DOUBLE , elem , node , NULL , NULL);
      } else if ( Match("pow(") ) {
        AV(4);
        ReadExpression(&l_t);
        if (EC!=',') SetError((char *)", expected",current);
        AV();
        ReadExpression(&r_t);
        AddNode( OPER_POW , elem , node , l_t , r_t);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    case 'M':
    case 'm': if ( Match("mod(") ) {
        AV(4);
        ReadExpression(&l_t);
        if (EC!=',') SetError((char *)", expected",current);
        AV();
        ReadExpression(&r_t);
        AddNode( OPER_MOD , elem , node , l_t , r_t);
        if (EC!=')') SetError((char *)") expected",current);
        AV();
      } else if ( Match("min(") ) {
        ParseFn("min",OPER_MIN,node);
      } else if ( Match("max(") ) {
        ParseFn("max",OPER_MAX,node);
      } else {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      }
      break;

    default: if( (EC>='A' && EC<='Z') ||
                 (EC>='a' && EC<='z') ||
                 (EC=='_'))
      {
        ReadName((char *)elem.name);
        AddNode( OPER_NAME , elem , node , NULL , NULL);
      } else
        SetError((char *)"Syntax error",current);
  }

}

// -------------------------------------------------------

void ExpParser::ReadExpression(ETREE **node) {

  ETREE *l_t=NULL;
  ETREE *r_t=NULL;
  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  ReadFactor1(&l_t);
  if (EC=='&')
  {
    AV();
    ReadFactor1(&r_t);
    AddNode( OPER_AND , elem , node , l_t , r_t );
  } else if (EC=='|') {
    AV();
    ReadFactor1(&r_t);
    AddNode( OPER_OR , elem , node , l_t , r_t );
  } else if (EC=='^') {
    AV();
    ReadFactor1(&r_t);
    AddNode( OPER_XOR , elem , node , l_t , r_t );
  } else {
    *node=l_t;
  }

}

// -------------------------------------------------------

void ExpParser::ReadFactor1(ETREE **node) {

  ETREE *l_t=NULL;
  ETREE *r_t=NULL;
  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  ReadFactor2(&l_t);
  if ((EC=='<') && Match("<=") )
  {
    AV(2);
    ReadFactor2(&r_t);
    AddNode( OPER_LWEQ , elem , node , l_t , r_t );
  } else if ((EC=='<') && !Match("<=") && !Match("<<") ) {
    AV();
    ReadFactor2(&r_t);
    AddNode( OPER_LW , elem , node , l_t , r_t );
  } else if ((EC=='>') && Match(">=") ) {
    AV(2);
    ReadFactor2(&r_t);
    AddNode( OPER_GTEQ , elem , node , l_t , r_t );
  } else if ((EC=='>') && !Match(">=") && !Match(">>") ) {
    AV();
    ReadFactor2(&r_t);
    AddNode( OPER_GT , elem , node , l_t , r_t );
 } else if ((EC=='=') && Match("==") ) {
    AV(2);
    ReadFactor2(&r_t);
    AddNode( OPER_EQ , elem , node , l_t , r_t );
  } else if ((EC=='!') && Match("!=") ) {
    AV(2);
    ReadFactor2(&r_t);
    AddNode( OPER_NEQ , elem , node , l_t , r_t );
  } else {
    *node=l_t;
  }

}

// -------------------------------------------------------

void ExpParser::ReadFactor2(ETREE **node) {

  ETREE *l_t=NULL;
  ETREE *r_t=NULL;
  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  ReadFactor3(&l_t);
  if ((EC=='<') && Match("<<"))
  {
    AV(2);
    ReadFactor3(&r_t);
    AddNode( OPER_LSHIFT , elem , node , l_t , r_t );
  } else if ( (EC=='>') && Match(">>") ) {
    AV(2);
    ReadFactor3(&r_t);
    AddNode( OPER_RSHIFT , elem , node , l_t , r_t );
  } else {
    *node=l_t;
  }

}

// -------------------------------------------------------

void ExpParser::ReadFactor3(ETREE **node)
{
  ETREE *l_t=NULL;
  ETREE *r_t=NULL;
  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  ReadFactor4(&l_t);

  while(EC=='+' || EC=='-')
  {
    switch(EC) {
    case '+': AV();
              ReadFactor4(&r_t);
              AddNode( OPER_PLUS , elem , &l_t , l_t , r_t );
              break;

    case '-': AV();
              ReadFactor4(&r_t);
              AddNode( OPER_MINUS , elem , &l_t , l_t , r_t );
              break;
    }
  }
  *node=l_t;

}

// -------------------------------------------------------

void ExpParser::ReadFactor4(ETREE **node)
{
  ETREE *l_t=NULL;
  ETREE *r_t=NULL;
  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  ReadTerm(&l_t);

  while(EC=='*' || EC=='/')
  {
    switch(EC) {
    case '*': AV();
              ReadTerm(&r_t);
              AddNode( OPER_MUL , elem , &l_t , l_t , r_t );
              break;

    case '/': AV();
              ReadTerm(&r_t);
              AddNode( OPER_DIV , elem , &l_t , l_t , r_t );
              break;
    }
  }
  *node=l_t;

}

// -------------------------------------------------------

void ExpParser::ParseAttribute()
{

  exprLgth = strlen(expr);
  if( exprLgth==0 )
    SetError((char*)"Empty expression");

  current=0;
  EC=expr[0];

  safe_free_tree(&evalTree);

  ReadName(name);
  if(EC!='=') SetError((char *)"= expected",current);AV();
  ReadExpression(&evalTree);
  if(current!=exprLgth)
    SetError((char *)"Syntax error",current);

}

// -------------------------------------------------------

void ExpParser::ParseAttFn(std::string fnName,CALCFN fn, ETREE **node) {

  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));

  int idx;
  AV(fnName.length()+1);
  if(Match("IgnoreNAN")) {
    elem.attinfo.ignoreNAN = true;
    AV(9);
    if (EC!=',') SetError((char *)", expected",current);
    AV();
  }
  ReadInteger(&idx);
  elem.attinfo.attLgth = 1;
  elem.attinfo.param.attList[0] = idx;
  while( EC==',' && elem.attinfo.attLgth<64 ) {
    AV();
    ReadInteger(&idx);
    elem.attinfo.param.attList[elem.attinfo.attLgth]=idx;
    elem.attinfo.attLgth++;
  }
  if( EC==',' ) SetError((char *)"List too long",current);
  AddNode( fn , elem , node , NULL , NULL);
  if (EC!=')') SetError((char *)") expected",current);
  AV();

}

void ExpParser::ParseFn(std::string fnName,CALCFN fn,ETREE **node) {

  ETREE_NODE elem;
  memset(&elem,0,sizeof(ETREE_NODE));
  AV(fnName.length()+1);
  if(Match("IgnoreNAN")) {
    elem.attinfo.ignoreNAN = true;
    AV(9);
    if (EC!=',') SetError((char *)", expected",current);
    AV();
  }
  int idx;
  ReadInteger(&idx);
  elem.attinfo.idx = idx;
  elem.attinfo.valIdx = 0;
  AddNode( fn , elem , node , NULL , NULL);
  if (EC!=')') SetError((char *)") expected",current);
  AV();

}

// -------------------------------------------------------

VALUE ExpParser::EvalTree(ETREE *t) {

  VALUE a,b;
  if(t->left)  a=EvalTree(t->left);
  if(t->right) b=EvalTree(t->right);
  return t->calc_fn(this,&(t->info),&a,&b);

}

// -------------------------------------------------------

double ExpParser::ReadAttribute(char *attName) {
  
  return parent->read_self_attribute(attName);

}

// -------------------------------------------------------

Tango::DeviceProxy *ExpParser::Import(string devName) {

  bool found = false;
  int i = 0;

  while( !found && i<(int)devices.size() ) {
    found = devName.compare(devices[i].devName)==0;
    if(!found) i++;
  }

  if(!found) {
    DEV_ITEM it;
    it.ds = new Tango::DeviceProxy(devName);
    it.devName = devName;
    devices.push_back(it);
    return it.ds;    
  } else {
    return devices[i].ds;
  }

}

// -------------------------------------------------------

Tango::DevULong ExpParser::GetIntegerValue(double value) {

  Tango::DevULong v = UI(value);
  return v;

}

// -------------------------------------------------------

double ExpParser::ReadExternAttribute(char *fullAttName) {

  char devNameStr[128];
  strcpy(devNameStr,fullAttName);
  char *attNameStr = strrchr(devNameStr,'/');
  if( attNameStr==NULL ) SetError((char *)"Malformed device name");
  *attNameStr=0;
  attNameStr++;

  string devName = string(devNameStr);
  string attName = string(attNameStr);

  Tango::DeviceProxy *ds = Import(devName);
  Tango::DeviceAttribute da = ds->read_attribute(attName);

  switch(da.get_type()) {

    case Tango::DEV_BOOLEAN:
    {
      Tango::DevBoolean v;
      da >> v;
      return (double)v;
    }
    break;
    case Tango::DEV_SHORT:
    {
      Tango::DevShort v;
      da >> v;
      return (double)v;
    }
    break;
    case Tango::DEV_USHORT:
    {
      Tango::DevUShort v;
      da >> v;
      return (double)v;
    }
    break;
    case Tango::DEV_LONG:
    {
      Tango::DevLong v;
      da >> v;
      return (double)v;
    }
    break;
    case Tango::DEV_ULONG:
    {
      Tango::DevULong v;
      da >> v;
      return (double)v;
    }
    break;
    case Tango::DEV_LONG64:
    {
      Tango::DevLong64 v;
      da >> v;
      return (double)v;
    }
      break;
    case Tango::DEV_ULONG64:
    {
      Tango::DevULong64 v;
      da >> v;
      return (double)v;
    }
      break;
    case Tango::DEV_DOUBLE:
    {
      Tango::DevDouble v;
      da >> v;
      return v;
    }
    break;   
    case Tango::DEV_FLOAT:
    {
      Tango::DevFloat v;
      da >> v;
      return (double)v;
    }
    break;   
    default:
       Tango::Except::throw_exception(
         (const char *)"AttributeComposer::error_read",
         (const char *)"Cannot read attribute (type not supported)",
         (const char *)"ExpParser::ReadExternAttribute");

  }

  return 0.0;

}

// -------------------------------------------------------

int ExpParser::GetCurrentPos() {
  return current;
}

// -------------------------------------------------------

char *ExpParser::GetStatus() {
  return status;
}

// -------------------------------------------------------

void ExpParser::EvaluateRead(VALUE *result) {
  errno=0;
  if(evalTree==NULL) SetError((char *)"Read evaluation expression not initialised");
  *result=EvalTree(evalTree);
}

} // namespace AttributeComposer_ns

