//+=============================================================================
//
// file :         AcqThread.h
//
// description :  Include for the AcqThread class.
//                This class is used for acquiring attribute data
//
// project :      AttributeComposer TANGO Device Server
//
// $Author: pons
//
// $Revision: $
//
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
#ifndef _ACQTHREAD_H
#define _ACQTHREAD_H

#include "AttributeComposer.h"

namespace AttributeComposer_ns {

class AcqThread : public omni_thread, public Tango::LogAdapter, public Tango::CallBack {


public:
    // Constructor.
    AcqThread(AttributeComposer *dsPtr, std::string &deviceName,std::string &attributeName, int acqPeriod,int acqBuffSize);
    ~AcqThread();
    void run(void *);
    std::string getError();
    std::string getString();
    std::vector<double> getBuffer();

private:

    omni_mutex mutex;

    AttributeComposer *ds;
    int period;
    int buffSize;
    time_t tickStart;

    Tango::DeviceProxy *dsSrc;
    std::string devName;
    std::string attName;
    std::string errStr;

    // Data
    std::vector<double> attBuffer;
    void addToBuffer(double d);
    std::string stringValue;

    time_t get_ticks();

}; // class AcqThread

} // namespace AttributeComposer_ns

#endif // _ACQTHREAD_H
