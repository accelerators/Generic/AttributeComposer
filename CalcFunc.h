/*
  File:        CalcFunc.h
  Description: A simple expression parser
  Author:      J-L PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 */

//=============================================================================
//
// $Author: pons $
//
// $Revision: 1.2 $
// $Date: $
//
// $HeadURL: $
//
//=============================================================================

/* Calculation functions */

#define UI(x) ((unsigned int)(floor(x)))
#define US(x) ((unsigned short)(floor(x)))
#define S(x) ((short)(floor(x)))
#define I(x) ((int)(floor(x)))

//== Utils Function ===========================================================

const double LOG2 = log(2.0);

static double fact(double x) {

  unsigned int f = UI(x);
  double r = 1.0;
  for(unsigned int i=1;i<=f;i++) {
    r = (double)i * r;
  }
  return (double)r;

}

//== Calculation Function =====================================================

VALUE OPER_PLUS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value + b->value;
  return r;
}

VALUE OPER_MINUS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value - b->value;
  return r;
}

VALUE OPER_MUL(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value * b->value;
  return r;
}

VALUE OPER_DIV(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value / b->value;
  return r;
}

VALUE OPER_OR(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = UI(a->value) | UI(b->value);
  return r;
}

VALUE OPER_AND(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = UI(a->value) & UI(b->value);
  return r;
}

VALUE OPER_XOR(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = UI(a->value) ^ UI(b->value);
  return r;
}

VALUE OPER_LW(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value < b->value;
  return r;
}

VALUE OPER_LWEQ(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value <= b->value;
  return r;
}

VALUE OPER_GT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value > b->value;
  return r;
}

VALUE OPER_GTEQ(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value >= b->value;
  return r;
}

VALUE OPER_EQ(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value == b->value;
  return r;
}

VALUE OPER_NEQ(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = a->value != b->value;
  return r;
}

VALUE OPER_LSHIFT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = UI(a->value) << UI(b->value);
  return r;
}

VALUE OPER_RSHIFT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = UI(a->value) >> UI(b->value);
  return r;
}

VALUE OPER_COS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = cos(a->value);
  return r;
}

VALUE OPER_POW(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = pow(a->value, b->value);
  return r;
}

VALUE OPER_MOD(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = fmod(a->value, b->value);
  return r;
}

VALUE OPER_FACT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = fact(a->value);
  return r;
}

VALUE OPER_ACOS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = acos(a->value);
  return r;
}

VALUE OPER_SIN(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = sin(a->value);
  return r;
}

VALUE OPER_ASIN(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = asin(a->value);
  return r;
}

VALUE OPER_COSH(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = cosh(a->value);
  return r;
}

VALUE OPER_SINH(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = sinh(a->value);
  return r;
}

VALUE OPER_EXP(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = exp(a->value);
  return r;
}

VALUE OPER_LN(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = log(a->value);
  return r;
}

VALUE OPER_LOG10(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = log10(a->value);
  return r;
}

VALUE OPER_LOG2(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = log(a->value) / LOG2;
  return r;
}

VALUE OPER_INV(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = 1.0 / a->value;
  return r;
}

VALUE OPER_SQRT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = sqrt(a->value);
  return r;
}

VALUE OPER_TAN(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = tan(a->value);
  return r;
}

VALUE OPER_ATAN(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = atan(a->value);
  return r;
}

VALUE OPER_ABS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = fabs(a->value);
  return r;
}

VALUE OPER_TANH(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = tanh(a->value);
  return r;
}

VALUE OPER_MINUS1(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = -a->value;
  return r;
}

VALUE OPER_NEG(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = ~UI(a->value);
  return r;
}

VALUE OPER_NOT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = !UI(a->value);
  return r;
}

VALUE OPER_SUM(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  r.value = 0.0;
  for (size_t i = 0; i < buff.size(); i++) {
    if (info->attinfo.ignoreNAN) {
      if (!isnan(buff[i]))
        r.value += buff[i];
    } else {
      r.value += buff[i];
    }
  }
  return r;
}

VALUE OPER_AVG(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  r.value = 0.0;
  int nbValid = 0;
  for(size_t i=0;i<buff.size();i++) {
    if (info->attinfo.ignoreNAN) {
      if(!isnan(buff[i])) {
        r.value += buff[i];
        nbValid++;
      }
    } else {
      r.value += buff[i];
      nbValid++;
    }
  }
  if(nbValid<=0) {
    r.value = NAN;
  } else {
    r.value /= (double) nbValid;
  }
  return r;
}

VALUE OPER_STD(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  double avg = 0.0;
  int nbValid = 0;
  for(size_t i=0;i<buff.size();i++) {
    if (info->attinfo.ignoreNAN) {
      if(!isnan(buff[i])) {
        avg += buff[i];
        nbValid++;
      }
    } else {
      avg += buff[i];
      nbValid++;
    }
  }
  if(nbValid<=0) {
    r.value = NAN;
    return r;
  }

  avg /= (double)nbValid;

  double sum2 = 0.0;
  for(size_t i=0;i<buff.size();i++) {
    if(info->attinfo.ignoreNAN) {
      if(!isnan(buff[i]))
        sum2 += (avg - buff[i]) * (avg - buff[i]);
    } else {
      sum2 += (avg - buff[i]) * (avg - buff[i]);
    }
  }
  sum2 /= (double)nbValid;
  r.value = sqrt(sum2);
  return r;
}

VALUE OPER_MIN(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  r.value = 1e99;
  for(size_t i=0;i<buff.size();i++)
    if(buff[i]<r.value) r.value = buff[i];
  return r;
}

VALUE OPER_MAX(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  r.value = -1e99;
  for(size_t i=0;i<buff.size();i++)
    if(buff[i]>r.value) r.value = buff[i];
  return r;
}

VALUE OPER_VAL(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  size_t vIdx = (size_t)info->attinfo.valIdx;
  if( buff.size()==0 ) obj->SetError((char *)("Att #" + std::to_string(info->attinfo.idx) + ", buffer empty").c_str());
  if( vIdx>=buff.size() ) obj->SetError((char *)("Att #" + std::to_string(info->attinfo.idx) + ", requested index out of bounds").c_str());
  r.value = buff[vIdx];
  return r;
}

VALUE OPER_LENGTH(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  vector<double> buff = obj->parent->getBuffer(info->attinfo.idx);
  if( info->attinfo.ignoreNAN ) {
    int nbValue = 0;
    for(size_t i=0;i<buff.size();i++)
      if(!isnan(buff[i])) nbValue++;
    r.value = (double)nbValue;
  } else {
    r.value = (double) buff.size();
  }
  return r;
}

VALUE OPER_SUMATT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = 0.0;
  int nbValid = 0;
  for(int i=0;i<info->attinfo.attLgth;i++) {
    vector<double> buff = obj->parent->getBuffer(info->attinfo.param.attList[i]);
    size_t bSize = buff.size();
    if(bSize>0) {
      if( info->attinfo.ignoreNAN ) {
        if(!isnan(buff[bSize-1])) {
          r.value += buff[bSize - 1];
          nbValid++;
        }
      } else {
        r.value += buff[bSize-1];
        nbValid++;
      }
    }
  }
  if(nbValid<=0)
    r.value = NAN;
  return r;
}

VALUE OPER_AVGATT(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = 0.0;
  int nbValid = 0;
  for(int i=0;i<info->attinfo.attLgth;i++) {
    vector<double> buff = obj->parent->getBuffer(info->attinfo.param.attList[i]);
    size_t bSize = buff.size();
    if(bSize>0) {
      if( info->attinfo.ignoreNAN ) {
        if(!isnan(buff[bSize-1])) {
          r.value += buff[bSize - 1];
          nbValid++;
        }
      } else {
        r.value += buff[bSize-1];
        nbValid++;
      }
    }
  }
  if(nbValid>0)
    r.value /= (double)nbValid;
  else
    r.value = NAN;
  return r;
}

VALUE OPER_CONTAINS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  std::string s = obj->parent->getString(info->attinfo.idx);
  if( s.find(info->attinfo.param.searchStr) == string::npos )
    r.value = 0;
  else
    r.value = 1;
  return r;
}

VALUE OPER_ICONTAINS(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  std::string s1 = obj->parent->getString(info->attinfo.idx);
  std::string s2 = info->attinfo.param.searchStr;
  transform(s1.begin(), s1.end(), s1.begin(),
            [](unsigned char c){ return std::tolower(c); });
  transform(s2.begin(), s2.end(), s2.begin(),
            [](unsigned char c){ return std::tolower(c); });
  if( s1.find(s2) == string::npos )
    r.value = 0;
  else
    r.value = 1;
  return r;
}

VALUE OPER_XATTR(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = obj->ReadExternAttribute(info->name);
  return r;
}

VALUE OPER_DOUBLE(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  r.value = info->constant;
  return r;
}

VALUE OPER_NAME(ExpParser *obj, ETREE_NODE *info, VALUE *a, VALUE *b) {
  VALUE r;
  // Read external attribute
  r.value = obj->ReadAttribute(info->name);
  return r;
}

