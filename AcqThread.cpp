//+=============================================================================
//
// file :         AcqThread.h
//
// description :  Include for the AcqThread class.
//                This class is used for acquiring attribute data
//
// project :      AttributeComposer TANGO Device Server
//
// $Author: pons
//
// $Revision: $
//
// copyleft :     European Synchrotron Radiation Facility
//                BP 220, Grenoble 38043
//                FRANCE
//
//-=============================================================================
namespace AttributeComposer_ns {
class AcqThread;
}

#include "AcqThread.h"


namespace AttributeComposer_ns {

// Constructor:
AcqThread::AcqThread(AttributeComposer *dsPtr, std::string &deviceName,std::string &attributeName, int acqPeriod,int acqBuffSize) :
        Tango::LogAdapter(dsPtr), devName(deviceName), attName(attributeName), ds(dsPtr), period(acqPeriod), buffSize(acqBuffSize)  {
  INFO_STREAM << "AcqThread::LoopThread(): entering." << endl;
  tickStart = -1;

  try {

    dsSrc = new Tango::DeviceProxy(devName);

  } catch (Tango::DevFailed &e) {

    cerr << "Warning, cannot import " << devName << endl;
    cerr << e.errors[0].desc << endl;
    cerr << devName +"/" + attName + " acquisition not started" << endl;
    errStr = e.errors[0].desc;
    return;

  }

  errStr = "";

  start();
}

// ----------------------------------------------------------------------------------------
// Destructor
// ----------------------------------------------------------------------------------------
AcqThread::~AcqThread() {

}

void sleepHRT(time_t ms) {
  struct timespec ts;
  ts.tv_sec = ms / 1000;
  ts.tv_nsec = (ms%1000) * 1000000;
  clock_nanosleep(CLOCK_MONOTONIC, 0, &ts, NULL);
}

//----------------------------------------------------------------------------
// Main acquisition loop
//----------------------------------------------------------------------------

void AcqThread::run(void *arg) {

  while (true) {

    time_t t0 = get_ticks();

    try {

      // Read data
      Tango::DeviceAttribute dAtt = dsSrc->read_attribute(attName);
      dAtt.reset_exceptions(Tango::DeviceAttribute::isempty_flag);
      if( dAtt.is_empty() )
        Tango::Except::throw_exception("AttrInvalid",
                                       "Attribute not found or invalid",
                                       "AcqThread::run");

      int type = dAtt.get_type();

      switch (type) {

        case Tango::DEV_DOUBLE: {
          Tango::DevDouble d;
          dAtt >> d;
          addToBuffer(d);
        };
          break;

        case Tango::DEV_FLOAT: {
          Tango::DevFloat d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_LONG: {
          Tango::DevLong d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_ULONG: {
          Tango::DevULong d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_LONG64: {
          Tango::DevLong64 d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_ULONG64: {
          Tango::DevULong64 d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_SHORT: {
          Tango::DevShort d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_USHORT: {
          Tango::DevUShort d;
          dAtt >> d;
          addToBuffer((double) d);
        };
          break;

        case Tango::DEV_STRING: {
          dAtt >> stringValue;
        };
          break;

      }

      errStr = "";

    } catch (Tango::DevFailed &e) {

      switch(ds->errorHandling) {
        case 0: // Reading errors trig exception (default)
          errStr = string(e.errors[0].desc);
          break;
        case 1: // Reading errors replaced by NaN
          errStr = string(e.errors[0].desc);
          addToBuffer(NAN);
          break;
        case 2: // Reading errors and NaN ignored
          break;
      }

    }

    // Wait until next iteration
    time_t t1 = get_ticks();
    time_t toSleep = (int) (period - (t1 - t0));
    if (toSleep > 0) sleepHRT(toSleep);

  }

}

//----------------------------------------------------------------------------
// Returns a copy of the acquisition buffer
//----------------------------------------------------------------------------
std::vector<double> AcqThread::getBuffer() {
  vector<double> ret;
  mutex.lock();
  ret = attBuffer;
  mutex.unlock();
  return ret;
}

//----------------------------------------------------------------------------
// Returns error string, empty string when ok
//----------------------------------------------------------------------------
std::string AcqThread::getError() {

  string retStr="";
  mutex.lock();
  if(errStr.length()>0)
    retStr = devName + "/" + attName + ":" + errStr;
  mutex.unlock();
  return retStr;

}

//----------------------------------------------------------------------------
// Returns value string
//----------------------------------------------------------------------------
std::string AcqThread::getString() {

  string retStr="";
  mutex.lock();
  retStr = stringValue;
  mutex.unlock();
  return retStr;

}

//----------------------------------------------------------------------------
// Add data to buffer
//-----------------------------------------------------------------------------
void AcqThread::addToBuffer(double d) {

  mutex.lock();
  attBuffer.push_back(d);
  if((int)attBuffer.size()>buffSize)
    attBuffer.erase(attBuffer.begin());
  mutex.unlock();
  //cout << devName << ":" << attName << ": " << attBuffer.size() << endl;

}

//----------------------------------------------------------------------------
// Return number of milliseconds
//-----------------------------------------------------------------------------
time_t AcqThread::get_ticks() {

  if (tickStart < 0)
    tickStart = time(NULL);

  struct timeval tv;
  gettimeofday(&tv, NULL);
  return ((tv.tv_sec - tickStart) * 1000 + tv.tv_usec / 1000);

}

} // namespace AttributeComposer_ns
