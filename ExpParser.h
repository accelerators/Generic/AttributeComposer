/*
  File:        ExpParser.h
  Description: A simple expression parser
  Author:      J-L PONS

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
*/

/* Supported grammar :                                                                                                */
/*                                                                                                                    */
/* <attdef> ::= <name> '=' <expr>                                                                                     */
/*                                                                                                                    */
/* <expr>   ::= <factor1> [ <oper1> <factor1> ]                                                                       */
/* <factor1>::= <factor2> [ <oper2> <factor2> ]                                                                       */
/* <factor2>::= <factor3> [ <oper3> <factor3> ]                                                                       */
/* <factor3>::= <factor4> [ <oper4> <factor4> ]*                                                                      */
/* <factor4>::= <term> [ <oper5> <term> ]*                                                                            */
/*                                                                                                                    */
/* <term>   ::= '(' <exp> ')'      |                                                                                  */
/*                <double>         |                                                                                  */
/*                <name>           |                                                                                  */
/*               '-' <term>        |                                                                                  */
/*               '~' <term>        |                                                                                  */
/*               'not ' <term>     |                                                                                  */
/*               'sin(' <exp> ')'  |                                                                                  */
/*               'cos(' <exp> ')'  |                                                                                  */
/*               'tan(' <exp> ')'  |                                                                                  */
/*               'asin(' <exp> ')' |                                                                                  */
/*               'acos(' <exp> ')' |                                                                                  */
/*               'atan(' <exp> ')' |                                                                                  */
/*               'abs(' <exp> ')'  |                                                                                  */
/*               'sinh(' <exp> ')' |                                                                                  */
/*               'cosh(' <exp> ')' |                                                                                  */
/*               'tanh(' <exp> ')' |                                                                                  */
/*               'exp(' <exp> ')'  |                                                                                  */
/*               'ln(' <exp> ')'   |                                                                                  */
/*               'log(' <exp> ')'  |                                                                                  */
/*               'inv(' <exp> ')'  |                                                                                  */
/*               'sqrt(' <exp> ')' |                                                                                  */
/*               'fact(' <exp> ')'  |                                                                                 */
/*               'pow(' <exp>,<exp> ')'  |                                                                            */
/*               'mod(' <exp>,<exp> ')'  |                                                                            */
/*                                                                                                                    */
/*               'xattr(' <string> ')' |   (Extern tango att)                                                         */
/*                                                                                                                    */
/*                                                                                                                    */
/*        'sumAtt(' [IgnoreNaN,]<attIdx>[,<attIdx>]* ')' |  (Sum of the last buffer value)                            */
/*        'avgAtt(' [IgnoreNaN,]<attIdx>[,<attIdx>]* ')' |  (Avg of the last buffer value)                            */
/*        'sum(' [IgnoreNaN,]<attIdx> ')' |    (Sum of the acquisition buffer)                                        */
/*        'avg(' [IgnoreNaN,]<attIdx> ')' |    (Avg of the acquisition buffer)                                        */
/*        'std(' [IgnoreNaN,]<attIdx> ')' |    (StdDev of the acquisition buffer)                                     */
/*        'min(' [IgnoreNaN,]<attIdx> ')' |    (Min of the acquisition buffer)                                        */
/*        'max(' [IgnoreNaN,]<attIdx> ')' |    (Max of the acquisition buffer)                                        */
/*        'length(' [IgnoreNaN,]<attIdx> ')' |   (Length value of the acquisition buffer)                             */
/*        'val(' <attIdx>,<valIdx> ')' |   (Nth value of the acquisition buffer)                                      */
/*        'contains(' <attIdx> , <string> ')'  | (Returns 1 is source attribute attIdx contains the specified string  */
/*                                                string, 0 otherwise)                                                */
/*        'icontains(' <attIdx> , <string> ')' | (Returns 1 is source attribute attIdx contains the specified string  */
/*                                                string (case unsensitive), 0 otherwise)                             */
/*                                                                                                                    */
/*               'PI'                                                                                                 */
/*                                                                                                                    */
/* <oper1>    ::= '&' | '|' | '^'                          lowest precedence                                          */
/* <oper2>    ::= '<' | '>' | '>=' | '<=' | '==' | '!='                                                               */
/* <oper3>    ::= '<<' | '>>'                                                                                         */
/* <oper4>    ::= '+' | '-'                                                                                           */
/* <oper5>    ::= '*' | '/'                                highest precedence                                         */
/*                                                                                                                    */
/*                                                                                                                    */
/* <double>   ::= <nb>* '.' <nb> <nb>* ['E' [-] <nb> <nb>*] | <integer>                                               */
/* <integer>  ::= ['0x']<nb>*                                                                                         */
/* <attIdx>   ::= <integer>                                                                                           */
/* <valIdx>   ::= <integer>                                                                                           */
/* <nb>       ::= '0'..'9'                                                                                            */
/* <name>     ::= <letter>[<letter> | <nb>]*                                                                          */
/* <letter>   ::= 'A'..'Z' | 'a'..'z' | '_'                                                                           */
/* <string>   ::= '\'' [<char>]* '\''                                                                                 */
/*                                                                                                                    */
//================================================================================
//
// $Author: pons $
//
// $Revision: 1.2 $
// $Date: $
//
// $Log: $
//
//=============================================================================

#ifndef _EXPPARSERH_
#define _EXPPARSERH_

#ifndef TEST
#include <AttributeComposer.h>

namespace AttributeComposer_ns
{
#else
#include <tango.h>
using namespace std;
#endif

#define MAXLENGHT       256  // Maximum name length

// value type
typedef struct _TVALUE {
  double   value;
} VALUE;

typedef union _ATTPARAM {
  int32_t attList[MAXLENGHT/4];  // List of source attribute (sumAtt)
  char    searchStr[MAXLENGHT];  // contains function param
} ATTPARAM;

// reg info
typedef struct _ATTINFO {
  int idx;            // Source attribute index
  int valIdx;         // Index in source attribute buffer
  int attLgth;        // Source attribute list length
  bool ignoreNAN;     // Ignore NAN
  ATTPARAM param;     // Function parameters
} ATTINFO;

//Evaluation tree node info
typedef union {
  ATTINFO  attinfo;
  char     name[MAXLENGHT];
  double   constant;
} ETREE_NODE;

// Calculation function
class ExpParser;
typedef VALUE (*CALCFN)(ExpParser *obj,ETREE_NODE *info,VALUE *a,VALUE *b);

//Evaluation tree node
typedef struct _ETREE {
  CALCFN         calc_fn;
  ETREE_NODE     info;
  struct _ETREE *left;
  struct _ETREE *right;
} ETREE;

// Device factory
typedef struct {
  Tango::DeviceProxy *ds;
  string devName;
} DEV_ITEM;

class ExpParser {

public:

  // Construct an expression parser
  // modbusDS: Handle to the modbus device
  // selfDS: Handle to device proxy on self (for attribute reading)
  ExpParser();
#ifndef TEST
  ExpParser(AttributeComposer *parent);
#endif
  ~ExpParser();

  // Formula name and type
  char *GetName();
  char *GetStatus();
  bool  GetBoolResult(VALUE r);

  // Expression management
  void  SetExpression(char *expr);   // Set expression
  char *GetExpression();             // Get the expression
  void  ParseAttribute();            // Compile attribute expression
  int   GetCurrentPos();             // Current parsing cursor position

  // Evaluation
  void   EvaluateRead(VALUE *result);  // Evaluate the read expression
  Tango::DevULong GetIntegerValue(double value);
  
  vector<double> ReadAttBuffer( int attIdx );
  double ReadAttribute(char *attName);
  double ReadExternAttribute(char *attName);

  // Error function
  void   SetError(char *err,int p=-1);

  // Temporary value
  Tango::DevDouble  dValue;

#ifndef TEST
    AttributeComposer *parent; // Parent
#endif

private:

  VALUE  EvalTree(ETREE *t);
  void   ReadExpression(ETREE **node);
  void   ReadTerm(ETREE **node);
  void   ReadFactor1(ETREE **node);
  void   ReadFactor2(ETREE **node);
  void   ReadFactor3(ETREE **node);
  void   ReadFactor4(ETREE **node);

  void   ReadName( char *name );
  void   ReadString(  char *str );
  void   ReadAttName( char *name );
  void   ReadDouble(double *R);
  void   ReadInteger(int *R);
  void   AddNode(CALCFN fn,ETREE_NODE info,ETREE **t,ETREE *left,ETREE *right);
  void   AV();
  void   AV(int n);
  bool   Match(string word);
  bool   IsLetter(char c);
  void   ParseAttFn(std::string fnName,CALCFN fn,ETREE **node);
  void   ParseFn(std::string fnName,CALCFN fn,ETREE **node);
  Tango::DeviceProxy *Import(string devName);

  char name[MAXLENGHT];   // Attribute or State Name
  long type;              // Attribute data type
  char status[512];       // status string
  Tango::DevState state;  // Inner state
  char expr[2028];        // Expression
  char EC;                // Current char
  int  current;           // Current char index
  int  exprLgth;          // Expression length

  ETREE *evalTree;
  vector<DEV_ITEM> devices;

};

#ifndef TEST
} // namespace AttributeComposer_ns
#endif

#endif /* _EXPPARSERH_ */
