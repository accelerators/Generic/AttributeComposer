# Attribute composer

AttributeComposer is an C++ class that allows to make calculation on source attributes and create dynamic attributes from the calculation results.<br/>
Here is the description of the device properties:

**SourceAttributes** List of source attributes that will be used in calculation (4 fields per line).

DeviceName,AttName,Polling period (ms),Buffer length

*DeviceName*: Device Name<br/>
*AttName*: Attribute Name<br/>
*Polling period*: The sampling period in milliseconds, (this is not the Tango polling period but the refresh rate of the internal acquisition buffer)<br/>
*Buffer length*: Length of the sampling buffer in number of sample<br/>


**DynamicAttributes** List of dynacmic attributes

A dynamic attribute can be defined using the following grammar.

**ErrorHandling** Error management behavior

0 => Reading errors trig exception (default)

1 => Reading errors replaced by NaN

2 => Reading errors and NaN ignored

```
/* Supported grammar :                                                                                                */
/*                                                                                                                    */
/* <attdef> ::= <name> '=' <expr>                                                                                     */
/*                                                                                                                    */
/* <expr>   ::= <factor1> [ <oper1> <factor1> ]                                                                       */
/* <factor1>::= <factor2> [ <oper2> <factor2> ]                                                                       */
/* <factor2>::= <factor3> [ <oper3> <factor3> ]                                                                       */
/* <factor3>::= <factor4> [ <oper4> <factor4> ]*                                                                      */
/* <factor4>::= <term> [ <oper5> <term> ]*                                                                            */
/*                                                                                                                    */
/* <term>   ::= '(' <exp> ')'      |                                                                                  */
/*                <double>         |                                                                                  */
/*                <name>           |                                                                                  */
/*               '-' <term>        |                                                                                  */
/*               '~' <term>        |                                                                                  */
/*               'not ' <term>     |                                                                                  */
/*               'sin(' <exp> ')'  |                                                                                  */
/*               'cos(' <exp> ')'  |                                                                                  */
/*               'tan(' <exp> ')'  |                                                                                  */
/*               'asin(' <exp> ')' |                                                                                  */
/*               'acos(' <exp> ')' |                                                                                  */
/*               'atan(' <exp> ')' |                                                                                  */
/*               'abs(' <exp> ')'  |                                                                                  */
/*               'sinh(' <exp> ')' |                                                                                  */
/*               'cosh(' <exp> ')' |                                                                                  */
/*               'tanh(' <exp> ')' |                                                                                  */
/*               'exp(' <exp> ')'  |                                                                                  */
/*               'ln(' <exp> ')'   |                                                                                  */
/*               'log(' <exp> ')'  |                                                                                  */
/*               'inv(' <exp> ')'  |                                                                                  */
/*               'sqrt(' <exp> ')' |                                                                                  */
/*               'fact(' <exp> ')'  |                                                                                 */
/*               'pow(' <exp>,<exp> ')'  |                                                                            */
/*               'mod(' <exp>,<exp> ')'  |                                                                            */
/*                                                                                                                    */
/*               'xattr(' <string> ')' |   (Extern tango att)                                                         */
/*                                                                                                                    */
/*                                                                                                                    */
/*        'sumAtt(' [IgnoreNaN,]<attIdx>[,<attIdx>]* ')' |  (Sum of the last buffer value)                            */
/*        'avgAtt(' [IgnoreNaN,]<attIdx>[,<attIdx>]* ')' |  (Avg of the last buffer value)                            */
/*        'sum(' [IgnoreNaN,]<attIdx> ')' |    (Sum of the acquisition buffer)                                        */
/*        'avg(' [IgnoreNaN,]<attIdx> ')' |    (Avg of the acquisition buffer)                                        */
/*        'std(' [IgnoreNaN,]<attIdx> ')' |    (StdDev of the acquisition buffer)                                     */
/*        'min(' [IgnoreNaN,]<attIdx> ')' |    (Min of the acquisition buffer)                                        */
/*        'max(' [IgnoreNaN,]<attIdx> ')' |    (Max of the acquisition buffer)                                        */
/*        'length(' [IgnoreNaN,]<attIdx> ')' |   (Length value of the acquisition buffer)                             */
/*        'val(' <attIdx>,<valIdx> ')' |   (Nth value of the acquisition buffer)                                      */
/*        'contains(' <attIdx> , <string> ')'  | (Returns 1 is source attribute attIdx contains the specified string  */
/*                                                string, 0 otherwise)                                                */
/*        'icontains(' <attIdx> , <string> ')' | (Returns 1 is source attribute attIdx contains the specified string  */
/*                                                string (case unsensitive), 0 otherwise)                             */
/*                                                                                                                    */
/*               'PI'                                                                                                 */
/*                                                                                                                    */
/* <oper1>    ::= '&' | '|' | '^'                          lowest precedence                                          */
/* <oper2>    ::= '<' | '>' | '>=' | '<=' | '==' | '!='                                                               */
/* <oper3>    ::= '<<' | '>>'                                                                                         */
/* <oper4>    ::= '+' | '-'                                                                                           */
/* <oper5>    ::= '*' | '/'                                highest precedence                                         */
/*                                                                                                                    */
/*                                                                                                                    */
/* <double>   ::= <nb>* '.' <nb> <nb>* ['E' [-] <nb> <nb>*] | <integer>                                               */
/* <integer>  ::= ['0x']<nb>*                                                                                         */
/* <attIdx>   ::= <integer>                                                                                           */
/* <valIdx>   ::= <integer>                                                                                           */
/* <nb>       ::= '0'..'9'                                                                                            */
/* <name>     ::= <letter>[<letter> | <nb>]*                                                                          */
/* <letter>   ::= 'A'..'Z' | 'a'..'z' | '_'                                                                           */
/* <string>   ::= '\'' [<char>]* '\''                                                                                 */
/*                                                                                                                    */
```

The attIdx is the index in the SourceAttributes list.

Example:<br/>

The following example create a Voltage attribute that is the sum of the 13 cavities.

![Example 1](img/img1.jpg)

The following example create a moving average filter on the beam position (X,Y) on the last 100 samples with a sampling rate of 100ms.

![Example 2](img/img2.jpg)




